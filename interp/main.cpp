#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdio>
#include <vector>
#include "fundec.h"

int main( int argc, char* argv[] )
{
	if ( argc )
	{
		using namespace interpretator;

		std::ifstream in(argv[1]);
		std::vector<std::string> text;
		readFile( in, text );
		in.close();
		deleteComments( text );
		for ( auto &it : text ) printf( "%s\n", it.c_str(  ) );
		deleteEmptyness( text );
		findAndSaveFunctions( text );
		//saveFunc(text[0]);
	}

	return 0;
}
