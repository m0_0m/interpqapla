#ifndef MEM_H
#define MEM_H

#include <sstream>
#include <string>
#include <vector>
#include <map>

namespace interpretator
{
	std::map<
		std::string,
		std::pair<std::vector<std::string>, std::string > > func;
}

#endif

