#ifndef FUNDEC_H
#define FUNDEC_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include "mem.h"

namespace interpretator
{
	std::string::size_type getCloserPos(
			std::string& str,
			std::string::size_type openerPos,
			char closer )
	{
		char opener = str[openerPos];
		unsigned long score = 1, bracketsInBetwean = 0;
		std::string::size_type it = 0, itmp = 0;
		std::string tmp = str.substr( openerPos + 1, str.length(  ) - openerPos );
		while ( score != 0 && it != std::string::npos && tmp.length() != 0 ) {
			std::string::size_type tmpPos = tmp.find_first_of( closer );
			it = tmp.find_first_of( opener );
			if ( it > tmpPos ) {
				it = tmpPos;
				--score;
			} else {
				++score;
			}
			itmp += it + 1;
			tmp = tmp.substr( it + 1, tmp.length(  ) - it );
			++bracketsInBetwean;
		}
		return itmp + openerPos - 1;
	}

	void deletePrefWhiteSpace( std::string& str )
	{
		std::string::size_type end = std::string::npos, size = str.length(  );
		do {
			++end;
			if( str[end] != ' ' && str[end] != '\n' && str[end] != '\t' ) {
				break;
			}
		} while ( end < size );
		if ( end != 0 ) {
			str.erase(0,end);
		}
	}

	void deleteBackWhiteSpace( std::string& str )
	{
		std::string::size_type start = str.length(  ), end = start;
		while ( start != std::string::npos ) {
			--start;
			if( str[start] != ' ' && str[start] != '\n' && str[start] != '\t' ) {
				break;
			}
		}
		if ( start != end ) {
			str.erase( start + 1, end );
		}
	}

	void getListOfDevidetThings(
			std::string& str,
			std::vector<std::string>& args,
			std::string::size_type start,
			std::string::size_type end,
		  	char devider )
	{
		std::string tmp = str.substr( start + 1, end - start - 1 );
		std::stringstream s;
		while ( tmp.size(  ) ) {
			start = 0;
			end = tmp.find_first_of( devider );
			int not_end = 1;
			if ( end == std::string::npos ) {
				end = tmp.length(  );
				not_end = 0;
			}
			std::string ttmp = tmp.substr( start, end - start );
			deletePrefWhiteSpace( ttmp );
			deleteBackWhiteSpace( ttmp );
			args.push_back( ttmp.c_str(  ) );
			s << tmp.substr( start, end - start );
			tmp.erase( start, end + not_end );
		}
	}
	
	std::string::size_type countStr( std::string& str, const std::string& c )
	{
		std::string::size_type it = std::string::npos, N = 0;
		do {
			it = str.find( c, it + 1 );
			if( it != std::string::npos ) ++N;
		} while ( it != std::string::npos );
		return N;
	}

	void bodyToLine(
			std::vector<std::string>& code,
			std::vector<std::string>::iterator& it )
	{
		auto start = it;
		long score = 1;
	  	long openers = 1, closers;
		while ( score != 0 ) {
			do {
				openers = countStr( *(it+1), "{" );
				closers = countStr( *(it+1), "}" );
				score += (openers - closers);
				if( score > 0 ) ++it;
			} while ( score > 0 && it+1 != code.end() ); 
		}
		++it;
		if( start != it - 1 ) {
			for ( auto i = start + 1; i <= it; ++i  ) {
				(*start) = (*start) + (*i);
			}
		}
		code.erase( start + 1, it + 1 );
		
		
		auto tmp = getCloserPos( *start, start->find_first_of( '{' ), '}' ) + 1;
		auto subs = start->substr(tmp+1,start->length()-tmp-1);
		start->erase(tmp+1, start->length()-tmp-1);
		if ( start + 1 != code.end() ){
			code.insert( start + 1, subs );
		} else {
			code.push_back( subs );
		}
		it = start;
	}

	void findAndSaveFunctions( std::vector<std::string>& code )
	{
		std::cout << std::endl;
		std::cout << "findAndSaveFunctions():\n" << std::endl;

		for ( auto it = code.begin(  ); it != code.end(  ); ++it ) {
			if ( it->find( "fun " ) != std::string::npos ) {
				while( it->find( "{" ) == std::string::npos ) {
					++it;
				}
				bodyToLine( code, it );
			}
			std::cout << *it << std::endl;
		}
	}

	void saveFunc( std::string &str )
	{
		std::stringstream s;
		s << str;
		std::string nameOfFunc;
		s >> nameOfFunc;
		if ( nameOfFunc == "fun" ) {
			s >> nameOfFunc;
			if ( nameOfFunc.find( '(' ) != std::string::npos ) {
				nameOfFunc.erase( nameOfFunc.find( '(' ), nameOfFunc.length(  ) );
			}
			std::string::size_type start = str.find_first_of( '(' );
			std::string::size_type end = getCloserPos( str, start, ')' );
			std::vector<std::string> args;
			getListOfDevidetThings( str, args, start, end, ',' );
			std::vector<std::string> comands;
			start = str.find_first_of( '{' );
			end = getCloserPos( str, start, '}' );
			if( start != std::string::npos &&
					end != std::string::npos &&
					end != start &&
					end != start + 1 )
			{
				std::string body = str.substr( start + 1, end - start );
				func[nameOfFunc] = std::make_pair( args, body );
			}
		}
	}

	void deleteComments( std::vector<std::string>& text )
	{
		for ( auto & itext : text ) {
			std::string::size_type start = itext.find("//");
			if ( start != std::string::npos ) {
				itext.erase( start, itext.length() );
			}
		}
	}
	
	void deleteEmptyness( std::vector<std::string>& text )
	{
		for ( auto it = text.begin(  ); it != text.end(  ); ++it ) {
			if ( it->length(  ) ) {
				deletePrefWhiteSpace( *it );
				deleteBackWhiteSpace( *it );
			} else {
				text.erase( it );
				--it;
			}
		}
	}

	void readFile( std::ifstream& in, std::vector<std::string>& text )
	{
		while ( true ){
			std::string str;
			std::getline( in, str );
			if ( !in.eof(  ) ) text.push_back( str );
			else break;
		}
	}
}

#endif

