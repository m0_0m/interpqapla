#ifndef MODULE_3_H
#define MODULE_3_H

#include <unordered_set>
#include <strstream>
#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <algorithm>
#include <map>

void check( bool notShit, std::string&& logInfo )
{
	if ( !notShit ) {
		system( ( std::string( "echo " ) + logInfo + std::string( " >> errorLog.txt" ) ).c_str( ) );
		abort( );
	}
}

bool lexIsDigit( char c ) { return c > '0' && c < '9'; }

std::string::iterator lexFindCloser( std::string::iterator itOpener, std::string::iterator end, char closer )
{
	char opener = *itOpener;
	unsigned c = 1;
	auto it = itOpener;
	++it;
	while ( it != end && c != 0 ) {
		if ( *it == opener ) {
			++c;
		} else if ( *it == closer ) {
			--c;
		}
		++it;
	}
	return it;
}

/*template<typename T>
void getListOfDevidetThings( std::string& str, std::vector<T>& args, std::string::size_type start, std::string::size_type end, char devider )
{
	std::string tmp = str.substr( start + 1, end - start - 1 );
	std::stringstream s;
	while ( tmp.size( ) ) {
		start = 0;
		end = tmp.find_first_of( devider );
		int not_end = 1;
		if ( end == std::string::npos ) {
			end = tmp.length( );
			not_end = 0;
		}
		std::string ttmp = tmp.substr( start, end - start );
		deletePrefWhiteSpace( ttmp );
		deleteBackWhiteSpace( ttmp );
		args.push_back( ttmp.c_str( ) );
		s << tmp.substr( start, end - start );
		tmp.erase( start, end + not_end );
	}
}*/



unsigned lexCount( std::string& from, std::string& what )
{
	unsigned c = 0;
	auto it = std::begin( from ), end = std::end( from );
	auto wIt = std::begin( what ), wEnd = std::end( what );
	unsigned size = wEnd - wIt, s = 0;
	while ( it != end ) {
		if ( size != s ) {
			if ( *it == *( wIt + s ) ) {
				++s;
			} else {
				s = 0;
			}
		} else {
			s = 0;
			++c;
		}
		++it;
	}
	if ( size == s ) {
		++c;
	}

	return c;
}
unsigned lexCount( std::string& from, std::string& what, std::vector<unsigned>& fidPos )
{
	unsigned c = 0;
	auto it = std::begin( from ), end = std::end( from );
	auto wIt = std::begin( what ), wEnd = std::end( what );
	unsigned size = wEnd - wIt, s = 0;
	while ( it != end ) {
		if ( size != s ) {
			if ( *it == *( wIt + s ) ) {
				++s;
			} else {
				s = 0;
			}
		} else {
			fidPos.push_back( it - std::begin( from ) );
			s = 0;
			++c;
		}
		++it;
	}
	if ( size == s ) {
		fidPos.push_back( it - std::begin( from ) );
		++c;
	}

	return c;
}


#endif