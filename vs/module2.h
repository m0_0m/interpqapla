#ifndef MODULE_2_H
#define MODULE_2_H
#include "module3.h"


struct Term {
	char val;
};
struct Fid :Term {
	std::string id;
	Fid( ) { this->val = 'f'; }
	Fid( const char* _id ) : id( _id ) { this->val = 'f'; }
	~Fid( ) {}
};
struct Id :Term {
	std::string id;
	Id( ) { this->val = 'i'; }
	Id( const char* _id ) : id( _id ) { this->val = 'i'; }
	~Id( ) {}
};
struct Statement :Term {
	Statement( ) { this->val = 's'; }
	~Statement( ) {}
};
struct Assign :Statement {
	Assign( ) { this->val = '='; }
	~Assign( ) {}
};
struct Expr :Statement {
	Expr( ) { this->val = 'e'; }
	~Expr( ) {}
};
struct N :Expr {
	long long num = 0;
	N( ) { this->val = 'n'; }
	~N( ) {}
};
struct Call :Expr {
	Call( ) { this->val = 'c'; }
	~Call( ) {}
};
struct BinOp :Expr {
	std::shared_ptr<Term> left, right;
	BinOp( ) { this->val = 'b '; }
	~BinOp( ) {}
};
struct UnOp :Expr {
	std::shared_ptr<Term> right;
	UnOp( ) { this->val = 'u'; }
	~UnOp( ) {}
};
struct Brackets :Expr {
	Brackets( ) { this->val = '('; }
	~Brackets( ) {}
};

struct Body :Statement {
	std::vector< std::shared_ptr<Statement> > statement;
	Body( ) { this->val = 'B'; }
	~Body( ) {}
};

struct If :Statement {
	Body body;
	std::shared_ptr<Expr> expression;
	If( ) { this->val = '?'; }
	~If( ) {}
};
struct Return :Statement {
	Return( ) { this->val = 'r'; }
	~Return( ) {}
};
struct Assert :Statement {
	Assert( ) { this->val = 'a'; }
	~Assert( ) {}
};


struct Func :Term
{
	std::vector<Id> id;
	Body body;
	Func( ) { this->val = 'F'; }
};

#endif