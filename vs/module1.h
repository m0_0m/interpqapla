#ifndef MODULE_1_H
#define MODULE_1_H
#include <unordered_set>
#include <iostream>
#include <string>
#include <sstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>
#include "module2.h"

std::map< Fid, Func > func;

void lexGetParts( std::string& str, std::string::iterator begin, std::string::iterator end, std::vector<Id>& args )
{
	auto it = begin;
	while ( it != end ) {
		if ( *it == ',' ) {
			args.push_back( str.substr( begin - std::begin( str ) + 1, it - begin - 1 ).c_str( ) );
			std::stringstream s;
			s << args.back( ).id;
			s >> args.back( ).id;
			begin = it;
			++it;
		}
		++it;
	}
	args.push_back( str.substr( begin - std::begin( str ) + 1, it - begin - 2 ).c_str( ) );
	std::stringstream s;
	s << args.back( ).id;
	s >> args.back( ).id;
}

void lexFindBody( std::string::iterator& begin, std::string::iterator& end )
{
	begin = std::find( begin, end, '{' );
	end = lexFindCloser( begin, end, '}' );
}

void lexFindIf(
	std::string& str,
	std::string::iterator& begin,
	std::string::iterator& end,
	std::string::iterator& conditionBegin,
	std::string::iterator& conditionEnd,
	std::string::iterator& bodyBegin,
	std::string::iterator& bodyEnd )
{
	auto pos = str.find( "if" );
	if ( pos == std::string::npos ) { begin = end; return; }
	begin += pos;
	if ( begin == end ) return;
	
	if (
		!( *( begin + 2 ) == ' ' || *( begin + 2 ) == '\n'
		|| *( begin + 2 ) == '\t' || *( begin + 2 ) == '(' ) ) {
		begin = end;
		return;
	}
	std::string::iterator jt( str.begin(  ) );
	pos = str.find( "then" );
	if( pos == std::string::npos || pos < begin - std::begin( str ) ) { begin = end; return; }
	jt += pos;
	if (
		jt == end ||
		!( *( jt - 1 ) == ' '  || *( jt - 1 ) == '\n'
		|| *( jt - 1 ) == '\t' || *( jt - 1 ) == ')'
		|| *( jt + 4 ) == ' '  || *( jt + 4 ) == '\n'
		|| *( jt + 4 ) == '\t' || *( jt + 4 ) == '{' ) ) {
		begin = end;
		return;
	}
	
	auto bb = jt, eb = end;
	lexFindBody( bb, eb );
	if ( bb == end || eb - 1 == end ) { begin = end; return; }

	end = eb;
	conditionBegin = begin + 2; conditionEnd = jt - 1;
	bodyBegin = bb; bodyEnd = eb;
}

void lexGetExpression( std::string& str, std::shared_ptr< Expr >& expr )
{

}

char lexGetTypeOfStetement( std::string& str, std::vector< std::shared_ptr<Statement> >& args )
{
	std::string::size_type begin1;
	auto itBegin1 = std::begin( str ), itEnd1 = std::end( str );
	if ( str.find( '{' ) != std::string::npos ) {
		lexFindBody( itBegin1, itEnd1 );
		begin1 = itBegin1 - std::begin( str );
	} else begin1 = std::string::npos;
	
	auto itBegin2 = std::begin( str ), itEnd2 = std::end( str );
	std::string::iterator bc, ec, bb, eb;
	lexFindIf( str, itBegin2, itEnd2, bc, ec, bb, eb );
	auto begin2 = ( itBegin2 != itEnd2 )? itBegin2 - std::begin( str ) : std::string::npos;
	
	auto begin3 = str.find( "assert " );

	auto begin4 = str.find( "return " );
	
	auto begin5 = str.find( ":=" );
	/////////////////////////////////////////////////////////////////////////////////////////////
	char ch;
	if ( !( begin1 == begin2 && begin2 == begin3 && begin3 == begin4 && begin4 == begin5 && begin5 == std::string::npos ) ) {
		if ( begin1 < begin2 ) ch = 'B'; else ch = '?';
		if ( std::min( begin1, begin2 ) > begin3 ) ch = 'a';
		if ( std::min( std::min( begin1, begin2 ), begin3 ) > begin4 ) ch = 'r';
		if ( std::min( std::min( std::min( begin1, begin2 ), begin3 ), begin4 ) > begin5 ) ch = '=';
	}
	if ( args.size( ) ) {
		if ( args.back( )->val == '?' && str.find( "else" ) != std::string::npos ) {
			ch = ':';
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////
	switch ( ch ) {
	case 'B': {
		Body* term;
		args.push_back( std::shared_ptr<Statement>( term = new Body( ) ) );
	}	break;
	case '?': {
		If* term;
		args.push_back( std::shared_ptr<Statement>( term = new If( ) ) );
		std::string expr( bc, ec );
		lexGetExpression( expr, term->expression );
		std::string bod( bb, eb );
		lexGetParts( bod, std::begin(bod), std::end(bod), term->body.statement );
	}	break;
	case 'a': {
		Assert* term;
		args.push_back( std::shared_ptr<Statement>( term = new Assert( ) ) );
	}	break;
	case 'r': {
		Return* term;
		args.push_back( std::shared_ptr<Statement>( term = new Return( ) ) );
	}	break;
	case '=': {
		Assign* term;
		args.push_back( std::shared_ptr<Statement>( term = new Assign( ) ) );
	} break;
	default:
		break;
	}

	return ch;
}

void lexGetParts( std::string& str, std::string::iterator begin, std::string::iterator end, std::vector< std::shared_ptr<Statement> >& args )
{
	auto it = begin+1;
	--end;
	unsigned c = 0;
	while ( it != end ) {
		if ( *it == '{' ) ++c;
		if ( *it == '}' ) --c;

		//std::cout << *it << std::endl;
		if ( (*it == ';' || *it == '}') && c == 0 ) {
			std::string str2 = str.substr( begin - std::begin( str ) + 1, it - begin - ( *it == ';' ) ).c_str( );
			std::cout << str2 << std::endl;
			std::cout << lexGetTypeOfStetement( str2, args ) << std::endl;
			begin = it;
			++it;
		}
		if ( it != end )
		++it;
	}
	//args.push_back( str.substr( begin - std::begin( str ) + 1, it - begin - 2 ).c_str( ) );
	//std::stringstream s;
	//s << args.back( ).id;
	//s >> args.back( ).id;
}

std::string line = "Func foo( id1, id2, id3, id4, id5 )  { if lol then{kek;kek;rofl;} else {kek:=101;} } Func goo2(  ){}";

void takeFuncDecls( std::string term )
{
	std::string keyWord = "Func ";
	std::vector<unsigned> fidPos;
	unsigned numOfDecls = lexCount( term, keyWord, fidPos );
	auto begin = std::begin( term ), end = std::end( term );
	for ( unsigned i = 0; i < numOfDecls; ++i ) {

		auto startName = begin + fidPos[i];
		auto rbrOpener = std::find( startName, end, '(' );
		check( rbrOpener != std::end( term ), "Unacceptable_Func_Declaration::Forgot_To_Open_Parentheses" );
		auto rbrCloser = lexFindCloser( rbrOpener, end, ')' );
		check( rbrCloser != std::end( term ), "Unacceptable_Func_Declaration::Forgot_To_Close_Parentheses" );

		std::string name( term.substr( startName - begin, rbrOpener - startName ) );
		{
			std::stringstream s;
			s << name;
			s >> name;
		}
		check( name.length( ), "Unacceptable_Func_Declaration::Forgot_To_Write_Name" );
		check( !lexIsDigit( name[0] ), "Unacceptable_Func_Declaration::Name_Of_Func::Cannot_Start_With_A_Digit" );

		Func* foo;
		std::shared_ptr<Term> f( foo = new  Func( ) );
		lexGetParts( term, rbrOpener, rbrCloser, foo->id );
		for ( auto& it : foo->id ) {
			check( !lexIsDigit( it.id[0] ), "Unacceptable_Func_Declaration::Name_Of_Argument::Cannot_Start_With_A_Digit" );
		}
		//
		rbrOpener = std::find( rbrCloser, end, '{' );
		rbrCloser = lexFindCloser( rbrOpener, end, '}' );
		lexGetParts( term, rbrOpener, rbrCloser, foo->body.statement );
	}
}


#endif