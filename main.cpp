#include <unordered_map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <unistd.h>

struct cinfo {
	std::string first, second;
	cinfo( const char* f, const char* s ) : first( f ), second( s ) {}
};
class fooInfo {
public:
	std::vector<std::string> args;
	std::string body;
	fooInfo(const std::vector<std::string>& ar,const char* b): args(ar),body(b){}
	fooInfo(){}
	~fooInfo(){}
};
std::unordered_map<std::string, long> var;
std::unordered_map<std::string, fooInfo> foo;
void read_whole_file(std::ifstream& in, std::string& str);
cinfo takeInfo( std::string str );
long getVar(std::string arg);
std::vector<long> takeArgs(std::string s);
std::vector<std::string> getStrArgs(std::string s);
void reduce( std::string& s, cinfo& inf, long c );
// Операторы
long plus( std::vector<long>& arg );
long deduct( std::vector<long>& arg );
long multiply( std::vector<long>& arg );
long devide( std::vector<long>& arg );
// Предикаты
long more( std::vector<long>& arg );
long moreEq( std::vector<long>& arg );
long less( std::vector<long>& arg );
long lessEq( std::vector<long>& arg );
long hence( std::vector<long>& arg );
long equal( std::vector<long>& arg );
long myAnd( std::vector<long>& arg );
long myOr( std::vector<long>& arg );
long myNot( std::vector<long>& arg );
//
bool step(std::string& str);
long analyse(std::string str);
bool init(std::string str);
void erase_all( char ch, std::string& str );
void space_after( char ch, std::string& str );
void space_before( char ch, std::string& str );
void find__all_brackeds(
		std::string& str,
		std::map<std::string::size_type,char>& um,
		char ch,
		std::string::size_type start_point,
		std::string::size_type end_point	);
std::pair<
	std::string::size_type,
	std::string::size_type> get__opener_n_closer(
							std::string& str,
							std::string::size_type _start_,
							std::string::size_type _end_ );

int main(  ) {
	std::string hello =
"\n                        s"
"\n                        o."
"\n                       ./:"
"\n                       -o/"
"\n                       :ho"
"\n                       /ms-"
"\n                      `smh:"
"\n                      :ymmo"
"\n                      +mmms-"
"\n                     -smmmd/"
"\n                     /dmmmms."
"\n                  `.:smmmmmd+-.`"
"\n               .:-.`/mmmmmmms.`-::."
"\n            `::.   -ymmmmmmmm/    ./-"
"\n           .:`    `+mmmmmmmmmh-     ./."
"\n          :-      /dmmmmmmmmmms.     `/-"
"\n         --      .ommmmmmmmmmmh:       /."
"\n        `/      . .ommmmmmmmmh: ``     ./"
"\n        --    `+s- `+mmmmmmmy: `+s.     +`"
"\n        --  `:ymmh: `/dmmmms- .ommh/`   /."
"\n        .:./ymmmmmd/` /hmmo. .smmmmmy/. +"
"\n        -ohmmmmmmmmd+` -yo. -ymmmmmmmmdyo-."
"\n      `/hmmmmmmmmmmmmo. .` :hmmmmmmmmmmmmdhyo/:--."
"\n     `+dmmmmmmmmmmmmdh+.  :ymmmmmmmmmmmmmmmmmmmho."
"\n     /dmmmmmmdys+:-..```  ...-:/oshhdmmmmmdhho:."
"\n     smmmmhs:--:.`              `-:-..----.."
"\n     smmy/-     .-:--.`````..-::.`"
"\n     od+.            `......`"
"\n     :/\n";
	std::string str;

	auto getStrArgsSpeshal = [] (std::string str) {
		unsigned start = 0, it = 0, dep = 0;
		std::vector<std::string> v;
		for (; it < str.length(  ); ++it  ) {
			if ( str[it] == '(' ) ++dep;
			if ( str[it] == ')' ) --dep;
			if ( dep == 0 && ( str[it] == ',' || it == str.length() - 1 ) ) {
				v.push_back(str.substr(start, it-start));
				start = it + 1;
			}
		}
		return v;
	};

	auto exe_list_of_comands = [&str, &getStrArgsSpeshal](  ){
		auto walls = get__opener_n_closer( str, 0, str.length() );
		std::string subs = str.substr( walls.first, walls.second - walls.first );
		std::vector<std::string> v = getStrArgsSpeshal( subs );
		for ( auto& it : v ) {
			std::cout << analyse( it ) << std::endl;
		}
	};

	while ( str != "exit" ) {
		std::cout << hello << std::endl;
		std::ifstream ifs( "comand.qph" );
		str = "";
		read_whole_file( ifs, str );
		erase_all( ' ', str );
		erase_all( '\t', str );
		erase_all( '\n', str );
		space_after( ',', str );
		space_after( '{', str );
		space_before( '}', str );
		std::cout << str << std::endl;
		if ( !init( str ) ) {
			exe_list_of_comands(  );
			//std::cout << analyse( str ) << std::endl;
		}
		ifs.close(  );
		sleep( 1 );
		system( "clear" );
	}

	return 0;
}


std::pair <std::string::size_type, std::string::size_type>
					get__opener_n_closer(
							std::string& str,
							std::string::size_type _start_,
							std::string::size_type _end_ )
{
	std::map<std::string::size_type, char> um;
	find__all_brackeds( str, um, '{', _start_, _end_ );
	find__all_brackeds( str, um, '}', _start_, _end_ );
	unsigned i = 0;
	for ( std::map<std::string::size_type,char>::iterator it = um.begin();
			it != um.end();
		  	it=std::next(it,1) )
	{
		if ( it->second == '{' ) {
			++i;
		} else if (it->second == '}') {
			--i;
		}
		if( i == 0 ) {
			return std::make_pair( um.begin()->first + 1, it->first );
		}
	}
	return std::make_pair( std::string::npos, std::string::npos );
}

void find__all_brackeds(
		std::string& str,
		std::map<std::string::size_type, char>& um,
		char ch,
		std::string::size_type start_point,
		std::string::size_type end_point	)
{
	std::string::size_type start = start_point, end = end_point;
	while( start < end)
	{
		if( std::string::npos!= str.substr(start, end).find_first_of(ch) ) {
			if (start != end) {
				start += str.substr(start, end).find_first_of(ch);
			}
			um.insert( std::make_pair(start, ch) );
			++start;
		}
		else break;
	}
}

void space_after( char ch, std::string& str ) {
	std::string::size_type start = 0, end = str.length(  ) - 1;
	while( start<end && str.substr(start,end-start).find(ch)!=std::string::npos )
	{
		start += str.substr(start, end-start).find( ch ) + 1;
		str.insert( start, " ");
		end++;
	}
}

void space_before( char ch, std::string& str ) {
	std::string::size_type start = 0, end = str.length(  ), hm;
	while ( start<end ) 
	{
		if( std::string::npos != (hm = str.substr( start, end-start ).find(ch)) )
		{
			str.insert( hm, " ");
			end++;
			start += str.substr(start, end-start).find( ch ) + 1;
		} else break;
	}
}
void erase_all( char ch, std::string& str ) {
	while(str.find(ch) != std::string::npos) str.erase(str.begin()+str.find(ch));
}

bool init( std::string str ) {
	auto eqpos = str.find_first_of("=");
	if( eqpos!=std::string::npos && str[eqpos+1]!= '=' && str[eqpos-1]!='!') {
		auto rBrStart = str.substr(0, eqpos).find("(");
		if ( rBrStart != std::string::npos )
		{// Инициализация функции
			auto rBrEnd = str.substr(rBrStart, eqpos).find_first_of(")");
			std::string name = str.substr(0, rBrStart);
			std::string body = str.substr(eqpos + 1, str.length() - eqpos - 1);
			fooInfo b(getStrArgs(str.substr(rBrStart+1,rBrEnd-1)),body.c_str());
			foo.insert(std::make_pair( name, b ));
		}
		else
		{// Инициализация переменной
			std::stringstream sm;
			std::string f;
			sm << str.substr(0, eqpos);
			sm >> f;
			long s = analyse(str.substr(eqpos + 1, str.length( ) - eqpos - 1));
			if ( var.find(f) == var.end( ) ) {
				var.insert( std::make_pair( f, s ) );
			} else {
				var[f] = s;
			}
			std::cout << f << " = " << var[f] << std::endl;
		}
		return true;
	}
	return false;
};

long analyse(std::string str) {
	long c = 0;
	//std::cout << str << std::endl;
	while(step(str)){
		//std::cout << str << std::endl;
	}
	//std::cout << str << std::endl;
	std::stringstream sm;
	sm << str;
	sm >> c;
	return c;
}

bool step(std::string& str) {
	if ( str.find("(") == std::string::npos ) { return false; }
	cinfo info = takeInfo( str );
	if(info.first != "") {
		std::vector<long> arg = takeArgs(info.second);
		if ( info.first == "+" ) {
			reduce( str, info, plus(arg) );
		} else if ( info.first == "-" ) {
			reduce( str, info, deduct(arg) );
		} else if ( info.first == "*" ) {
			reduce( str, info, multiply(arg) );
		} else if ( info.first == "/" ) {
			reduce( str, info, devide(arg) );
		} else if ( info.first == ">" ) {
			reduce( str, info, more(arg) );
		} else if ( info.first == ">=" ) {
			reduce( str, info, moreEq(arg) );
		} else if ( info.first == "<" ) {
			reduce( str, info, less(arg) );
		} else if ( info.first == "<=" ) {
			reduce( str, info, lessEq(arg) );
		} else if ( info.first == "==" ) {
			reduce( str, info, equal(arg) );
		} else if ( info.first =="->" || info.first=="=>" || info.first=="|=" ) {
			reduce( str, info, hence(arg) );
		} else if ( info.first == "and" || info.first=="и"||info.first== "&&" ) {
			reduce( str, info, myAnd(arg) );
		} else if ( info.first == "or" || info.first=="или"||info.first=="||" ) {
			reduce( str, info, myOr(arg) );
		} else if ( info.first == "!" || info.first=="not" ||info.first=="не" ) {
			reduce( str, info, myNot(arg) );
		} else if ( foo.find(info.first) != foo.end() ) {
			std::string tmp = foo[info.first].body;
			std::string a;
			for ( unsigned i = 0; i < foo[info.first].args.size(); ++i ) {
				a = foo[info.first].args[i];
				std::string astr = std::to_string( arg[i] );
				//std::cout << a << ", " << astr << std::endl;
				auto ap = tmp.find_first_of( a ); 
				auto united = [&tmp, &astr, &ap, &a](  ) {
					//auto ap = tmp.find_first_of(a) - 1;
					auto arsize = a.size( );
					//std::cout << "a= " << a << std::endl;
					char d1 = tmp[ap-1], d2 = tmp[ap + arsize];
					if ( //ap != std::string::npos
						/*	&&*/ ( d1== '('|| d1== ' '|| d1== ','|| d1=='\n'||
								  d1=='\t'|| d1== '{'|| d1== '['|| d1== '<' )
							&& ( d2== ')'|| d2== ' '|| d2== ','|| d2=='\n'||
								  d2=='\t'|| d2== '}'|| d2== ']'|| d2== '>' ) )
					{//если нашлась независимая переменная
						tmp.erase(ap, arsize);
						//std::cout<<"tmp.erase("<<ap<<","<<arsize<<')'<<tmp<< std::endl;
						tmp.insert(ap, astr);
						//std::cout<<"tmp.insert("<<ap<<","<<astr<<')'<<tmp<< std::endl;
					}
				};
				while ( ap != std::string::npos ) {
					united();
					ap = tmp.find_first_of( a );
				}
				//std::cout << i << std::endl;
				//std::cout << tmp << std::endl;
			}
			reduce( str, info, analyse(tmp) );
		}
		return true;
	}
	return false;
};

void reduce( std::string& s, cinfo& inf, long c ) {
	unsigned start = s.length(), end = 0;
	for( unsigned i = 0; i < s.length( ); ++i ) {
		if ( s[i] == ')' ) {
			end = i;
			break;
		}
	}
	for( unsigned i = end; i > 0; --i ) {
		if ( s[i] == '(' ) {
			start = i - inf.first.length( );
			break;
		}
	}
	s.erase( start, end - (start) +1 );
	//std::cout << "s.erase("<<start<<","<<end<<")"<< s << std::endl;
	s.insert( start, std::to_string( c ) );
	//std::cout << "s.insert("<<start<<","<<end<<")"<< s << std::endl;
}
////////////// Операторы
long plus(std::vector<long>& arg) {
	long res = 0;
	for (auto it : arg) {
		res += it;
	}
	return res;
}

long deduct(std::vector<long>& arg) {
	long res = arg.size()? arg[0] : 0;
	for ( unsigned i = 1; i < arg.size(); ++i ) {
		res -= arg[i];
	}
	return res;
}

long multiply(std::vector<long>& arg) {
	long res = ((bool)arg.size());
	for (auto it : arg) {
		res *= it;
	}
	return res;
}

long devide(std::vector<long>& arg) {
	long res = arg.size()? arg[0] : 0;
	for ( unsigned i = 1; i < arg.size(); ++i ) {
		res /= arg[i];
	}
	return res;
}
// Предикаты
long more( std::vector<long>& arg ) {
	return arg.size() == 2 ? arg[0] > arg[1] : false;
}

long moreEq( std::vector<long>& arg ) {
	return arg.size() == 2 ? arg[0] >= arg[1] : false;
}

long less( std::vector<long>& arg ) {
	return arg.size() == 2 ? arg[0] < arg[1] : false;
}

long lessEq( std::vector<long>& arg ) {
	return arg.size() == 2 ? arg[0] <= arg[1] : false;
}

long hence( std::vector<long>& arg ) {
	return arg.size() == 2 ? !((bool)arg[0]) || (bool)arg[1] : false;
}

long equal( std::vector<long>& arg ) {
	return arg.size() == 2 ? arg[0] == arg[1] : false;
}

long myAnd( std::vector<long>& arg ) {
	return arg.size() == 2 ? (bool)arg[0] && (bool)arg[1] : false;
}

long myOr( std::vector<long>& arg ) {
	return arg.size() == 2 ? (bool)arg[0] || (bool)arg[1] : false;
}

long myNot( std::vector<long>& arg ) {
	for ( auto it : arg ) if ( it != 0 ) return true;
	return false;
}
///////////////////////
std::vector<std::string> getStrArgs(std::string s) {
	std::vector<std::string> arg;
	std::stringstream sm;
	unsigned start = 0;
	std::string c = "";
	for ( unsigned i = 0; i < s.length( ); ++i ) {
		if ( s[i] == ',' || i == s.length( ) - 1 ) {
			sm << s.substr( start, i - start + 2 );
			sm >> c;
			if (c.back()==',') c.erase( c.length() - 1 );
			arg.push_back( c );
			c.clear( );
			sm.clear( );
			start = i + 1;
		}
	}
	return arg;
}

std::vector<long> takeArgs( std::string s ) {
	std::vector<long> arg;
	unsigned start = 0;
	std::string c = "";
	for ( unsigned i = 0; i < s.length(  ); ++i ) {
		if ( s[i] == ',' || i == s.length(  ) - 1 ) {
			std::stringstream sm;
			sm << s.substr( start, i - start + 2 );
			sm >> c;
			if (c.back()==',') c.erase( c.length() - 1 );
			arg.push_back( getVar( c ) );
			//std::cout<<"takeArgs::s= "<<s<<"; i="
			//<<i<<"; c="<<c<<'|'/*<< getVar(c)*/ << std::endl;
			c = "";
			sm.clear(  );
			start = i + 1;
		}
	}
	return arg;
}

long getVar( std::string arg ) {
	long c=0;
	{
		auto comma=arg.find(',');
		if(comma!=std::string::npos) { arg[arg.find(',')] = ' '; }
	}
	std::stringstream sm;
	sm << arg;
	sm >> arg;
	//std::cout << "getVar::arg= " << arg << std::endl;
	//sm.flush( );
	//sm.clear( );
	if ( arg[0] >= '0' && arg[0] <= '9' ) {
		std::stringstream sm2;
		sm2 << arg;
		sm2 >> c;
		//std::cout << "getVar::c= " << c << std::endl;
	} else if ( var.find(arg) != var.end( ) ) {
		c = var[arg];
	} else {
		//std::cout << arg << std::endl;
		c = 0;
	}
	return c;
}

cinfo takeInfo( std::string str ) {
	std::string f = "", s = "";
	str.insert( str.begin(), ' ' );
	long start = str.length( ), end = 0;
	for ( unsigned i = 0; i < str.length( ); ++i ) {
		if ( str[i] == ')' ) {
			end = i;
			break;
		}
	}
	for ( unsigned i = end; i > 0; --i ) {
		if ( str[i] == '(' ) {
			start = i;
			break;
		}
	}
	s = str.substr( start + 1, end - start - 1 );
	start -= 1;
	for ( int i = start; i > -1; i-- ) {
		if ( str[i]==' ' || str[i]=='(' || str[i]=='\n' ||
			  str[i]=='\t'|| str[i]==',' || i== 0 ) {
			end = start + 1;
			start = i+1;
			break;
		}
	}
	if ( start != end ) {
		f = str.substr( start, end - start );
	} else {
		f = "";
	}
	//std::cout << "takeInfo::f= " << f << "takeInfo::s= " << s << std::endl;
	return cinfo( f.c_str( ), s.c_str( ) );
}
void read_whole_file(std::ifstream& in, std::string& str) {
	in.seekg(0, std::ios::end);
	str.reserve(in.tellg());
	in.seekg(0, std::ios::beg);
	str.assign((std::istreambuf_iterator<char>(in)),
			std::istreambuf_iterator<char>());
}

